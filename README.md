## EU4 Combat Simulator ##
* Combat simulator for Europa Universalis IV by Paradox Interactive
* Version 1.2 (alpha)

### How do I get it? ###
* **Grab the executable jar from the [the drone.io Continuous Integration server](https://drone.io/bitbucket.org/Hottemax/eu4-combatsimulator/files)**

### How to contribute ###
Don't be afraid to hop in and contribute - everyone can help:

* Contribute code via git
* Ideas for new features
* Report bugs
* Point out wrongly modeled combat logic
* Code review

![EU4-CombatSimulator-1.2-GUI.png](https://bitbucket.org/repo/6GxEpA/images/543166591-EU4-CombatSimulator-1.2-GUI.png)
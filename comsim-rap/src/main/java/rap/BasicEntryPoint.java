package rap;

import org.eclipse.rap.rwt.application.AbstractEntryPoint;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import eu4.comsim.gui.MainComposite;

public class BasicEntryPoint
		extends AbstractEntryPoint {
	
	
	/**
	 * @wbp.parser.entryPoint
	 */
	@Override
	protected void createContents(Composite parent) {
		MainComposite mc = new MainComposite(parent, SWT.NONE);
	}
}

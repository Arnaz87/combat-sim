package eu4.comsim.core;

import java.util.Collection;
import java.util.stream.Collectors;

import eu4.comsim.core.datatypes.UnitType;

/**
 * Army statistics for evaluation.<br>
 *
 */
public class BattleStats {
	final int duration;
	final Collection<Regiment> regiments;
	
	/**
	 * Create a new stat instance
	 *
	 * @param regiments
	 *            The regiments to create stats for
	 * @param duration
	 *            Duration of the battle.
	 * @param copy
	 *            Whether or not to make a copy of the regiments passed in the constructor. (<code>true</code> is
	 *            usually used for logging/statkeeping, <code>false</code> if you want your BattleStats instance to
	 *            reflect live changes to the underlying collection (a.k.a. during an ongoing {@link Battle}
	 */
	public BattleStats(Collection<Regiment> regiments, int duration, boolean copy) {
		this.regiments = (copy) ? regiments.stream().map(Regiment::copy).collect(Collectors.toList()) : regiments;
		this.duration = duration;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public int originalStrength() {
		return Regiment.FULL_STRENGTH * regiments.size();
	}
	
	/**
	 * @return Sum of all the regiment's individual strength ( "Remaining warm bodies"). It does not matter whether or
	 *         not their morale is above zero.
	 * @see #routed()
	 * @see #fighting()
	 */
	public int strength() {
		return regiments.stream().mapToInt(Regiment::getStrength).sum();
	}
	
	/**
	 * @return Number of men still in battling shape (morale > 0). For the loser of a Battle this value will always be
	 *         <code>0</code>.
	 */
	public int fighting() {
		return strength() - routed();
	}
	
	/**
	 * @return RIP.
	 */
	public int casualties() {
		return originalStrength() - strength();
	}
	
	/**
	 * @return The man that fled the {@link Battleline} after their morale has been reduced to 0.
	 */
	public int routed() {
		return regiments.stream().filter(Regiment::routed).mapToInt(Regiment::getStrength).sum();
	}
	
	/**
	 * @return The arithmetic mean of the morale of all remaining soldiers.
	 */
	public double averageMorale() {
		return regiments.stream().mapToDouble(Regiment::getCurrentMorale).average().orElse(0);
	}
	
	/**
	 * @param type
	 *            The {@link UnitType} to retrieve stats for.
	 * @return A BattleStats instance that refers to the same objects (no-copy), that <i>filters</i> for all stats for
	 *         the given type only.
	 */
	public BattleStats adaptTo(UnitType type) {
		return new BattleStats(Regiment.getRegiments(regiments, type), duration, false);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Duration " + getDuration() + "\n");
		sb.append("Strength " + strength() + "\n");
		sb.append("Fighting " + fighting() + "\n");
		sb.append("Casualties " + casualties() + "\n");
		sb.append("Routed " + routed() + "\n");
		sb.append("Avg morale " + averageMorale() + "\n");
		return sb.toString();
	}
}
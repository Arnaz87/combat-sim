package eu4.comsim.core;

import java.io.Serializable;

import eu4.comsim.core.datatypes.Phase;

/**
 * Models a EU4 land leader
 *
 * @see <a href="http://www.eu4wiki.com/Military_leader">http://www.eu4wiki.com/ Military_leader</a>
 */
public class General implements Serializable {
	
	private static final long serialVersionUID = -7104995321796196883L;
	
	/** Fire pips (0-6), applied in {@link Phase#FIRE} */
	public final int fire;
	/** Shock pips (0-6), applied in {@link Phase#SHOCK} */
	public final int shock;
	/** Maneuver pips (0-6), applied in determining attacker crossing penalty */
	public final int maneuver;
	/** Siege pips (0-6), not relevant directly in combat */
	public final int siege;
	
	public General() {
		this(0, 0, 0, 0);
	}
	
	/**
	 * Creates a new General with the given stats; these are final
	 */
	public General(int fire, int shock, int maneuver, int siege) {
		this.fire = fire;
		this.shock = shock;
		this.maneuver = maneuver;
		this.siege = siege;
	}
	
	/**
	 * Pips for fire or shock. Not sensible to pass {@link Phase#MORALE} here
	 */
	public int getPips(Phase phase) {
		if (phase == Phase.MORALE) { throw new IllegalArgumentException("Only fire and shock allowed"); }
		return (phase == Phase.FIRE) ? fire : shock;
	}
	
	@Override
	public String toString() {
		return "General(" + fire + "," + shock + "," + maneuver + "," + siege + ")";
	}
	
	/**
	 * Computes die modifier based on General stats for relevant phase and terrain penalty
	 *
	 * @param opponent
	 *            Opponent general
	 * @param phase
	 *            Current phase
	 * @param environmentPenalty
	 *            (Positive) sum of all environmental penalties (terrain + crossing penalty)
	 * @return Difference between general phase pips minus environment penalty. Entire result may be negative, has to be
	 *         considered in {@link Regiment#attack(Regiment, Phase, int, Regiment, double, boolean)}
	 */
	public int difference(General opponent, Phase phase, int environmentPenalty) {
		int generalDifference = Math.max(0, getPips(phase) - opponent.getPips(phase));
		int dieRoll = generalDifference - environmentPenalty;
		return dieRoll;
	}
}

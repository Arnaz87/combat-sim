package eu4.comsim.core.datatypes;

import java.util.stream.Stream;

import eu4.comsim.core.Technology;
import eu4.comsim.gui.TechnologyGroup;

/**
 * Enumeration of all Units unlockable in the game.<br>
 *
 * @see <a href="http://www.eu4wiki.com/Land_units">http://www.eu4wiki.com/ Land_units</a>
 */
public enum Unit {
	
	ADAL_GUERILLA_WARFARE(23, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "East African Guerrillas", 3, 3, 2, 2, 2, 3),
	ADAL_GUNPOWDER_WARFARE(15, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "East African Musketeers", 2, 2, 2, 2, 2, 3),
	AFRICAN_ABYSSINIAN_CAVALRY(10, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Abyssinian Barded Cavalry", 0, 0, 3, 2, 3, 2),
	AFRICAN_ABYSSINIAN_LIGHT_CAVALRY(1, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Abyssinian Light Cavalry", 0, 0, 1, 1, 2, 0),
	AFRICAN_CLUBMEN(1, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Clubmen", 0, 0, 1, 0, 1, 1),
	AFRICAN_CUIRASSIER(28, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "African Cuirassier", 1, 1, 4, 4, 4, 4),
	AFRICAN_DRAGOON(23, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "African Dragoon", 1, 1, 3, 3, 4, 4),
	AFRICAN_HILL_WARFARE(12, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Hill Warriors", 2, 1, 1, 2, 2, 2),
	AFRICAN_HUSSAR(14, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "African Hussar", 1, 0, 3, 2, 3, 3),
	AFRICAN_MANDELAKU(1, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Mandekalu Cavalry", 0, 0, 1, 1, 1, 1),
	AFRICAN_MOSSI_HORSEMEN(10, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Mossi Horsemen", 0, 0, 2, 2, 3, 3),
	AFRICAN_SOMALI_CAVALRY(6, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Somali Light Cavalry", 0, 0, 2, 1, 1, 2),
	AFRICAN_SPEARMEN(1, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Spearmen", 0, 0, 0, 1, 1, 1),
	AFRICAN_SWARM(17, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "African Swarm Cavalry", 0, 1, 4, 3, 3, 2),
	AFRICAN_TUAREG_CAVALRY(6, Technology.Group.SUB_SAHARAN, UnitType.CAVALRY, "Tuareg Cavalry", 0, 0, 2, 1, 2, 1),
	AFRICAN_WESTERN_FRANCHISE_WARFARE(30, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Western Franchise Infantry", 3, 3, 3, 3, 4, 4),
	AFSHARID_REFORMED(18, Technology.Group.MUSLIM, UnitType.CAVALRY, "Afsharid Reformed Cavalry", 0, 1, 4, 3, 3, 3),
	AFSHARID_REFORMED_INFANTRY(15, Technology.Group.MUSLIM, UnitType.INFANTRY, "Afsharid Reformed Infantry", 3, 2, 2, 2, 2, 2),
	ALGONKIN_TOMAHAWK_CHARGE(5, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Algonkin Tomahawk Warriors", 0, 0, 2, 1, 2, 1),
	ANGLOFRENCH_LINE(23, Technology.Group.WESTERN, UnitType.INFANTRY, "Line Infantry", 3, 3, 2, 2, 3, 3),
	APACHE_GUERILLA(26, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "North American Guerrillas", 4, 3, 2, 4, 3, 3),
	ASIAN_ARQUEBUSIER(9, Technology.Group.CHINESE, UnitType.INFANTRY, "Asian Arquebusier", 1, 1, 1, 1, 1, 1),
	ASIAN_CHARGE_CAVALRY(14, Technology.Group.CHINESE, UnitType.CAVALRY, "Asian Charge Cavalry", 0, 1, 3, 2, 3, 3),
	ASIAN_MASS_INFANTRY(15, Technology.Group.CHINESE, UnitType.INFANTRY, "Asian Mass Infantry", 3, 2, 2, 2, 2, 2),
	ASIAN_MUSKETEER(19, Technology.Group.CHINESE, UnitType.INFANTRY, "Asian Musketeer", 3, 3, 2, 2, 3, 2),
	AUSTRIAN_GRENZER(23, Technology.Group.WESTERN, UnitType.INFANTRY, "Grenzer Infantry", 2, 3, 2, 3, 3, 3),
	AUSTRIAN_HUSSAR(23, Technology.Group.WESTERN, UnitType.CAVALRY, "Latin Hussars", 1, 2, 4, 3, 4, 4),
	AUSTRIAN_JAEGER(30, Technology.Group.WESTERN, UnitType.INFANTRY, "Jaeger Infantry", 4, 4, 4, 3, 3, 4),
	AUSTRIAN_TERCIO(19, Technology.Group.WESTERN, UnitType.INFANTRY, "Reformed Tercio", 2, 2, 2, 3, 3, 3),
	AUSTRIAN_WHITE_COAT(26, Technology.Group.WESTERN, UnitType.INFANTRY, "White Coat Infantry", 4, 3, 3, 3, 3, 4),
	AZTEC_GUNPOWDER_WARFARE(14, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "American Hill Musketeers", 2, 1, 3, 2, 2, 2),
	AZTEC_HILL_WARFARE(10, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Reformed American Hill Warriors", 0, 0, 3, 1, 2, 2),
	AZTEC_TRIBAL_WARFARE(5, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "American Hill Warriors", 0, 0, 2, 1, 2, 1),
	BANTU_GUNPOWDER_WARFARE(15, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "South African Musketeers", 3, 2, 3, 2, 2, 1),
	BANTU_PLAINS_WARFARE(12, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Plains Warriors", 2, 1, 2, 2, 2, 1),
	BANTU_TRIBAL_WARFARE(5, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "South African Warrior", 0, 0, 2, 1, 2, 1),
	BARDICHE_INFANTRY(1, Technology.Group.EASTERN, UnitType.INFANTRY, "Bardiche Infantry", 0, 0, 0, 0, 1, 1),
	BHONSLE_CAVALRY(28, Technology.Group.INDIAN, UnitType.CAVALRY, "Sowars", 1, 2, 4, 4, 4, 3),
	BHONSLE_INFANTRY(18, Technology.Group.INDIAN, UnitType.INFANTRY, "Deccani Musket Infantry", 3, 3, 2, 2, 2, 2),
	BRITISH_HUSSAR(26, Technology.Group.WESTERN, UnitType.CAVALRY, "Reformed Latin Hussars", 1, 2, 4, 4, 4, 4),
	BRITISH_REDCOAT(26, Technology.Group.WESTERN, UnitType.INFANTRY, "Redcoat Infantry", 3, 4, 3, 3, 4, 3),
	BRITISH_SQUARE(28, Technology.Group.WESTERN, UnitType.INFANTRY, "Square Infantry", 3, 4, 3, 3, 4, 4),
	CENTRAL_AMERICAN_DRAGOON(23, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "Central American Dragoon", 2, 2, 3, 3, 4, 4),
	CENTRAL_AMERICAN_HORSEMEN(6, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "Central American Horsemen", 0, 0, 2, 1, 1, 1),
	CENTRAL_AMERICAN_HUSSAR(14, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "Central American Hussar", 1, 1, 3, 2, 3, 2),
	CENTRAL_AMERICAN_RIFLE_CAVALRY(10, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "Central American Cavalry", 0, 1, 3, 2, 2, 2),
	CENTRAL_AMERICAN_SWARM(19, Technology.Group.MESOAMERICAN, UnitType.CAVALRY, "Central American Swarm Cavalry", 1, 2, 3, 3, 3, 2),
	CHAMBERED_DEMI_CANNON(16, Technology.Group.WESTERN, UnitType.ARTILLERY, "Chambered Demi-Cannon", 2, 2, 0, 1, 2, 1),
	CHEVAUCHEE(1, Technology.Group.WESTERN, UnitType.CAVALRY, "Chevauchée", 0, 0, 1, 1, 1, 0),
	CHINESE_DRAGOON(23, Technology.Group.CHINESE, UnitType.CAVALRY, "Asian Dragoons", 1, 2, 3, 3, 4, 3),
	CHINESE_FOOTSOLDIER(5, Technology.Group.CHINESE, UnitType.INFANTRY, "Offensive Asian Foot Soldier", 0, 0, 1, 0, 2, 1),
	CHINESE_LONGSPEAR(1, Technology.Group.CHINESE, UnitType.INFANTRY, "Asian Longspear Infantry", 0, 0, 0, 1, 0, 1),
	CHINESE_STEPPE(6, Technology.Group.CHINESE, UnitType.CAVALRY, "Asian Steppe Cavalry", 0, 0, 2, 2, 1, 1),
	COEHORN_MORTAR(22, Technology.Group.WESTERN, UnitType.ARTILLERY, "Coehorn Mortar", 3, 3, 1, 1, 3, 3),
	COMMANCHE_SWARM(19, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "North American Swarm Cavalry", 1, 2, 3, 3, 3, 2),
	CREEK_ARQUEBUSIER(14, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Creek Arquebusier", 2, 2, 2, 2, 2, 2),
	CULVERIN(10, Technology.Group.WESTERN, UnitType.ARTILLERY, "Culverin", 1, 1, 0, 1, 1, 0),
	DRUZHINA_CAVALRY(1, Technology.Group.EASTERN, UnitType.CAVALRY, "Druzhina Cavalry", 0, 0, 0, 1, 1, 1),
	DURRANI_DRAGOON(28, Technology.Group.MUSLIM, UnitType.CAVALRY, "Durrani Cavalry", 2, 1, 5, 3, 4, 4),
	DURRANI_RIFLED_MUSKETEER(23, Technology.Group.MUSLIM, UnitType.INFANTRY, "Reformed Muslim Musketeers", 1, 2, 3, 3, 4, 3),
	DURRANI_SWIVEL(28, Technology.Group.MUSLIM, UnitType.CAVALRY, "Durrani Swivel Cavalry", 1, 2, 4, 5, 4, 3),
	DUTCH_MAURICIAN(15, Technology.Group.WESTERN, UnitType.INFANTRY, "Maurician Infantry", 2, 2, 2, 1, 3, 2),
	EAST_ASIAN_SPEARMEN(1, Technology.Group.CHINESE, UnitType.INFANTRY, "East Asian Spearmen", 0, 0, 0, 1, 1, 0),
	EAST_MONGOLIAN_STEPPE(10, Technology.Group.CHINESE, UnitType.CAVALRY, "Reformed Asian Steppe Cavalry", 1, 0, 2, 2, 2, 2),
	EASTERN_BOW(1, Technology.Group.CHINESE, UnitType.CAVALRY, "East Asian Archer Cavalry", 0, 0, 1, 0, 1, 1),
	EASTERN_CARABINIER(26, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Eastern Carabinier", 4, 3, 3, 3, 3, 3),
	EASTERN_KNIGHTS(1, Technology.Group.EASTERN, UnitType.CAVALRY, "Eastern Knights", 0, 0, 1, 0, 1, 1),
	EASTERN_MEDIEVAL_INFANTRY(1, Technology.Group.EASTERN, UnitType.INFANTRY, "Eastern Medieval Infantry", 0, 0, 1, 0, 1, 0),
	EASTERN_MILITIA(5, Technology.Group.EASTERN, UnitType.INFANTRY, "Eastern Militia", 0, 0, 1, 1, 1, 1),
	EASTERN_SKIRMISHER(26, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Eastern Skirmisher", 1, 2, 4, 3, 4, 3),
	EASTERN_UHLAN(26, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Eastern Uhlan", 2, 1, 3, 4, 4, 3),
	ETHIOPIAN_GUERILLA_WARFARE(23, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "North African Guerrillas", 2, 3, 2, 3, 2, 3),
	ETHIOPIAN_GUNPOWDER_WARFARE(15, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "North African Musketeers", 2, 3, 2, 3, 1, 2),
	ETHIOPIAN_MOUNTAIN_WARFARE(12, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Mountain Warriors", 1, 2, 2, 2, 1, 2),
	FLYING_BATTERY(29, Technology.Group.WESTERN, UnitType.ARTILLERY, "Flying Battery", 4, 4, 2, 2, 4, 4),
	FRENCH_BLUECOAT(26, Technology.Group.WESTERN, UnitType.INFANTRY, "Blue Coat Infantry", 3, 3, 3, 3, 4, 4),
	FRENCH_CARABINIER(26, Technology.Group.WESTERN, UnitType.CAVALRY, "Carabiniers", 2, 1, 5, 4, 3, 4),
	FRENCH_CARACOLLE(14, Technology.Group.WESTERN, UnitType.CAVALRY, "Latin Caracole Cavalry", 1, 0, 3, 2, 2, 2),
	FRENCH_CUIRASSIER(28, Technology.Group.WESTERN, UnitType.CAVALRY, "Latin Cuirassiers", 1, 2, 5, 4, 4, 5),
	FRENCH_DRAGOON(23, Technology.Group.WESTERN, UnitType.CAVALRY, "Latin Dragoons", 1, 1, 4, 4, 4, 4),
	FRENCH_IMPULSE(28, Technology.Group.WESTERN, UnitType.INFANTRY, "Impulse Infantry", 4, 3, 3, 3, 4, 4),
	GAELIC_FREE_SHOOTER(12, Technology.Group.WESTERN, UnitType.INFANTRY, "Free Shooter Infantry", 1, 2, 2, 1, 3, 1),
	GAELIC_GALLOGLAIGH(5, Technology.Group.WESTERN, UnitType.INFANTRY, "Galloglaigh Infantry", 0, 0, 1, 0, 2, 0),
	GAELIC_MERCENARY(9, Technology.Group.WESTERN, UnitType.INFANTRY, "Reformed Galloglaigh Infantry", 0, 0, 2, 0, 2, 1),
	GERMANIZED_PIKE(9, Technology.Group.EASTERN, UnitType.INFANTRY, "Pike Infantry", 0, 0, 1, 2, 1, 2),
	HALBERD_INFANTRY(1, Technology.Group.WESTERN, UnitType.INFANTRY, "Halberd Infantry", 0, 0, 1, 0, 1, 0),
	HAN_BANNER(12, Technology.Group.CHINESE, UnitType.INFANTRY, "Banner Infantry", 1, 2, 2, 1, 2, 2),
	HOUFNICE(7, Technology.Group.WESTERN, UnitType.ARTILLERY, "Houfnice", 1, 0, 0, 0, 0, 1),
	HUNGARIAN_HUSSAR(10, Technology.Group.EASTERN, UnitType.CAVALRY, "Eastern Hussar", 0, 0, 2, 2, 3, 2),
	HURON_ARQUEBUSIER(14, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Native American Arquebusier", 2, 2, 2, 1, 3, 2),
	INCA_MOUNTAIN_WARFARE(5, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Reformed Mountain Warriors", 0, 0, 1, 2, 1, 2),
	INCAN_AXEMEN(5, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Incan Axemen", 0, 0, 1, 1, 2, 2),
	INCAN_GUERILLA_WARFARE(19, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Incan Guerrilla", 2, 3, 2, 2, 2, 3),
	INCAN_SLINGSHOTS(5, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Incan Slingshots", 0, 0, 2, 2, 1, 1),
	INDIAN_ARCHERS(1, Technology.Group.INDIAN, UnitType.CAVALRY, "Elephant Archers", 0, 0, 1, 0, 1, 1),
	INDIAN_ARQUEBUSIER(5, Technology.Group.INDIAN, UnitType.INFANTRY, "Indian Arquebusier", 1, 0, 1, 1, 1, 0),
	INDIAN_ELEPHANT(10, Technology.Group.INDIAN, UnitType.CAVALRY, "Mansabdar Cavalry", 1, 0, 3, 2, 2, 2),
	INDIAN_FOOTSOLDIER(1, Technology.Group.INDIAN, UnitType.INFANTRY, "Indian Foot Soldier", 0, 0, 1, 0, 1, 1),
	INDIAN_RIFLE(26, Technology.Group.INDIAN, UnitType.INFANTRY, "North Indian Sepoy", 3, 3, 3, 3, 3, 4),
	INDIAN_SHOCK_CAVALRY(23, Technology.Group.INDIAN, UnitType.CAVALRY, "Deccani Light Cavalry", 1, 1, 4, 3, 4, 3),
	IRISH_CHARGE(15, Technology.Group.WESTERN, UnitType.INFANTRY, "Charge Infantry", 1, 2, 3, 1, 3, 2),
	IROQUOIS_RIFLE_SCOUT(19, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Rifle Scout Infantry", 3, 3, 2, 2, 2, 2),
	ITALIAN_CONDOTTA(9, Technology.Group.WESTERN, UnitType.INFANTRY, "Condotta Infantry", 0, 0, 2, 1, 1, 1),
	JAPANESE_ARCHER(1, Technology.Group.CHINESE, UnitType.INFANTRY, "Asian Longbow", 0, 0, 1, 0, 0, 1),
	JAPANESE_FOOTSOLDIER(5, Technology.Group.CHINESE, UnitType.INFANTRY, "Defensive Asian Foot Soldier", 0, 0, 1, 1, 1, 1),
	JAPANESE_SAMURAI(6, Technology.Group.CHINESE, UnitType.CAVALRY, "Samurai Cavalry", 0, 0, 2, 1, 2, 1),
	LARGE_CAST_BRONZE_MORTAR(7, Technology.Group.WESTERN, UnitType.ARTILLERY, "Large Cast Bronze Mortar", 1, 0, 0, 0, 1, 0),
	LARGE_CAST_IRON_BOMBARD(13, Technology.Group.WESTERN, UnitType.ARTILLERY, "Large Cast Iron Cannon", 2, 1, 0, 0, 1, 2),
	LEATHER_CANNON(18, Technology.Group.WESTERN, UnitType.ARTILLERY, "Leather Cannon", 2, 2, 1, 1, 2, 2),
	MAHARATHAN_CAVALRY(17, Technology.Group.INDIAN, UnitType.CAVALRY, "Maratha Raiders", 0, 2, 4, 3, 3, 2),
	MAHARATHAN_GUERILLA_WARFARE(23, Technology.Group.INDIAN, UnitType.INFANTRY, "Telingas", 4, 3, 2, 2, 2, 3),
	MALI_TRIBAL_WARFARE(5, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "North African Warrior", 0, 0, 2, 2, 1, 1),
	MAMLUK_ARCHER(1, Technology.Group.MUSLIM, UnitType.INFANTRY, "Muslim Archer", 0, 0, 1, 1, 0, 1),
	MAMLUK_CAVALRY_CHARGE(1, Technology.Group.MUSLIM, UnitType.CAVALRY, "Charge Cavalry", 0, 0, 2, 0, 1, 1),
	MAMLUK_DUEL(5, Technology.Group.MUSLIM, UnitType.INFANTRY, "Muslim Duel Infantry", 0, 0, 1, 1, 1, 1),
	MAMLUK_MUSKET_CHARGE(18, Technology.Group.MUSLIM, UnitType.CAVALRY, "Musket Charge Cavalry", 1, 1, 4, 3, 3, 2),
	MANCHU_BANNER(17, Technology.Group.CHINESE, UnitType.CAVALRY, "Banner Cavalry", 1, 1, 3, 3, 3, 3),
	MAYA_FOREST_WARFARE(10, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Reformed American Forest Warriors", 0, 0, 2, 2, 2, 2),
	MAYA_GUERILLA_WARFARE(19, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "American Guerrilla Warfare", 3, 2, 3, 2, 2, 2),
	MAYA_GUNPOWDER_WARFARE(14, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "American Forest Musketeers", 2, 2, 2, 2, 2, 2),
	MAYA_TRIBAL_WARFARE(5, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "American Forest Warriors", 0, 0, 2, 1, 1, 2),
	MESOAMERICAN_SPEARMEN(1, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Mesoamerican Spearmen", 0, 0, 1, 0, 1, 1),
	MEXICAN_GUERILLA_WARFARE(19, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Central American Guerrillas", 2, 2, 2, 2, 3, 3),
	MIXED_ORDER_INFANTRY(30, Technology.Group.WESTERN, UnitType.INFANTRY, "Mixed Order Infantry", 4, 3, 4, 3, 4, 4),
	MONGOL_BOW(1, Technology.Group.NOMAD, UnitType.INFANTRY, "Eastern Archers", 0, 0, 1, 1, 1, 1),
	MONGOL_STEPPE(1, Technology.Group.NOMAD, UnitType.CAVALRY, "Eastern Steppe Cavalry", 0, 0, 1, 1, 2, 2),
	MONGOL_SWARM(1, Technology.Group.NOMAD, UnitType.CAVALRY, "Eastern Swarm Cavalry", 0, 0, 2, 1, 2, 1),
	MONGOLIAN_BOW(1, Technology.Group.CHINESE, UnitType.CAVALRY, "Archer Cavalry", 0, 0, 0, 1, 1, 1),
	MUGHAL_MANSABDAR(6, Technology.Group.INDIAN, UnitType.CAVALRY, "Indian Cavalry Archers", 0, 0, 2, 2, 1, 1),
	MUGHAL_MUSKETEER(9, Technology.Group.INDIAN, UnitType.INFANTRY, "Toofangchis", 1, 1, 1, 1, 2, 1),
	MUSCOVITE_CARACOLLE(14, Technology.Group.EASTERN, UnitType.CAVALRY, "Eastern Caracole", 2, 0, 2, 2, 3, 3),
	MUSCOVITE_COSSACK(22, Technology.Group.EASTERN, UnitType.CAVALRY, "Cossack Cavalry", 1, 1, 4, 3, 4, 4),
	MUSCOVITE_MUSKETEER(12, Technology.Group.EASTERN, UnitType.INFANTRY, "Offensive Eastern Musketeers", 2, 1, 2, 1, 3, 2),
	MUSCOVITE_SOLDATY(15, Technology.Group.EASTERN, UnitType.INFANTRY, "Soldaty Infantry", 3, 1, 2, 1, 3, 2),
	MUSLIM_CAVALRY_ARCHERS(1, Technology.Group.MUSLIM, UnitType.CAVALRY, "Muslim Cavalry Archers", 0, 0, 1, 1, 1, 1),
	MUSLIM_DRAGOON(23, Technology.Group.MUSLIM, UnitType.CAVALRY, "Muslim Dragoon", 1, 2, 3, 4, 3, 4),
	MUSLIM_MASS_INFANTRY(26, Technology.Group.MUSLIM, UnitType.INFANTRY, "Muslim Mass Infantry", 3, 4, 3, 3, 3, 3),
	NAPOLEONIC_LANCERS(28, Technology.Group.WESTERN, UnitType.CAVALRY, "Latin Lancers", 0, 2, 6, 4, 5, 4),
	NAPOLEONIC_SQUARE(30, Technology.Group.WESTERN, UnitType.INFANTRY, "Napoleonic Square", 4, 4, 4, 3, 4, 3),
	NATIVE_CLUBMEN(1, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Clubmen", 0, 0, 1, 0, 1, 1),
	NATIVE_INDIAN_ARCHER(1, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Native American Archer", 0, 0, 0, 1, 1, 1),
	NATIVE_INDIAN_HORSEMEN(6, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "North American Horsemen", 0, 0, 2, 1, 1, 1),
	NATIVE_INDIAN_MOUNTAIN_WARFARE(10, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Native American Mountain Warriors", 1, 1, 1, 2, 2, 1),
	NATIVE_INDIAN_TRIBAL_WARFARE(5, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "American Plains Warriors", 0, 0, 2, 1, 1, 2),
	NIGER_KONGOLESE_FOREST_WARFARE(12, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "African Forest Warriors", 1, 1, 2, 2, 2, 2),
	NIGER_KONGOLESE_GUERILLA_WARFARE(23, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Central African Guerrillas", 3, 2, 2, 3, 3, 2),
	NIGER_KONGOLESE_GUNPOWDER_WARFARE(15, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Central African Musketeers", 3, 2, 2, 2, 2, 2),
	NIGER_KONGOLESE_TRIBAL_WARFARE(5, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Central African Warrior", 0, 0, 1, 1, 2, 2),
	NORTH_AMERICAN_HUSSAR(14, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "North American Hussar", 1, 1, 3, 2, 3, 2),
	NORTH_AMERICAN_RIFLE_CAVALRY(10, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "North American Cavalry", 0, 1, 3, 2, 2, 2),
	OPEN_ORDER_CAVALRY(28, Technology.Group.WESTERN, UnitType.CAVALRY, "Latin Chasseur", 2, 1, 4, 4, 5, 5),
	OTTOMAN_AZAB(5, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Azab Infantry", 0, 0, 1, 1, 2, 1),
	OTTOMAN_JANISSARY(9, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Janissary Infantry", 1, 0, 1, 1, 2, 2),
	OTTOMAN_LANCER(28, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Reformed Lancer", 1, 1, 5, 3, 5, 3),
	OTTOMAN_MUSELLEM(1, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Musellem Cavalry", 0, 0, 2, 1, 1, 1),
	OTTOMAN_NEW_MODEL(30, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Eastern New Model Infantry", 3, 3, 3, 3, 4, 4),
	OTTOMAN_NIZAMI_CEDID(23, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Nizami Cedid Infantry", 2, 3, 2, 2, 3, 3),
	OTTOMAN_REFORMED_JANISSARY(19, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Reformed Janissary Infantry", 2, 2, 2, 2, 3, 2),
	OTTOMAN_REFORMED_SPAHI(18, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Reformed Spahi Cavalry", 1, 1, 3, 3, 3, 3),
	OTTOMAN_SEKBAN(12, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Sekban Infantry", 2, 2, 2, 1, 2, 3),
	OTTOMAN_SPAHI(10, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Spahi Cavalry", 0, 0, 3, 2, 3, 2),
	OTTOMAN_TIMARIOT(6, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Timariot Cavalry", 0, 0, 2, 1, 2, 1),
	OTTOMAN_TOPRAKLI_DRAGOON(28, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Toprakli Dragoons", 2, 2, 3, 3, 4, 4),
	OTTOMAN_TOPRAKLI_HIT_AND_RUN(23, Technology.Group.OTTOMAN, UnitType.CAVALRY, "Toprakli Hit and Run Cavalry", 2, 0, 4, 3, 4, 3),
	OTTOMAN_YAYA(1, Technology.Group.OTTOMAN, UnitType.INFANTRY, "Yaya Infantry", 0, 0, 1, 0, 1, 1),
	PEDRERO(10, Technology.Group.WESTERN, UnitType.ARTILLERY, "Pedrero", 1, 1, 0, 0, 1, 1),
	PERSIAN_CAVALRY_CHARGE(1, Technology.Group.MUSLIM, UnitType.CAVALRY, "Muslim Cavalry", 0, 0, 1, 0, 2, 1),
	PERSIAN_FOOTSOLDIER(1, Technology.Group.MUSLIM, UnitType.INFANTRY, "Muslim Foot Soldier", 0, 0, 1, 0, 1, 1),
	PERSIAN_RIFLE(30, Technology.Group.MUSLIM, UnitType.INFANTRY, "Muslim Rifle Infantry", 3, 3, 3, 3, 4, 4),
	PERSIAN_SHAMSHIR(9, Technology.Group.MUSLIM, UnitType.INFANTRY, "Shamshir Infantry", 0, 0, 2, 1, 2, 2),
	PERUVIAN_GUERILLA_WARFARE(19, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Guerrilla", 2, 2, 2, 2, 3, 3),
	POLISH_HUSSAR(14, Technology.Group.EASTERN, UnitType.CAVALRY, "Reformed Eastern Hussars", 1, 1, 3, 3, 2, 2),
	POLISH_MUSKETEER(12, Technology.Group.EASTERN, UnitType.INFANTRY, "Defensive Eastern Musketeers", 2, 2, 1, 2, 2, 2),
	POLISH_TERCIO(15, Technology.Group.EASTERN, UnitType.INFANTRY, "Eastern Tercio", 2, 2, 1, 2, 2, 3),
	POLISH_WINGED_HUSSAR(22, Technology.Group.EASTERN, UnitType.CAVALRY, "Winged Hussars", 0, 1, 5, 4, 4, 3),
	PRUSSIAN_DRILL(30, Technology.Group.WESTERN, UnitType.INFANTRY, "Drill Infantry", 4, 4, 3, 3, 4, 4),
	PRUSSIAN_FREDERICKIAN(26, Technology.Group.WESTERN, UnitType.INFANTRY, "Frederickian Infantry", 4, 3, 3, 3, 4, 3),
	PRUSSIAN_UHLAN(26, Technology.Group.WESTERN, UnitType.CAVALRY, "Uhlan Cavalry", 1, 2, 5, 4, 4, 3),
	PUEBLO_AMBUSH(5, Technology.Group.NORTH_AMERICAN, UnitType.INFANTRY, "Ambush Infantry", 0, 0, 1, 2, 2, 1),
	QIZILBASH_CAVALRY(10, Technology.Group.MUSLIM, UnitType.CAVALRY, "Qizilbash Cavalry", 0, 0, 2, 2, 3, 2),
	RAJPUT_HILL_FIGHTERS(1, Technology.Group.INDIAN, UnitType.CAVALRY, "Indian Cavalry", 0, 0, 0, 1, 1, 1),
	RAJPUT_MUSKETEER(12, Technology.Group.INDIAN, UnitType.INFANTRY, "Akbarid Musketeers", 2, 2, 1, 2, 2, 2),
	REFORMED_ASIAN_CAVALRY(28, Technology.Group.CHINESE, UnitType.CAVALRY, "Reformed Asian Cavalry", 1, 2, 5, 4, 3, 4),
	REFORMED_ASIAN_MUSKETEER(26, Technology.Group.CHINESE, UnitType.INFANTRY, "Reformed Asian Musketeer", 4, 4, 3, 3, 3, 3),
	REFORMED_MANCHU_RIFLE(28, Technology.Group.CHINESE, UnitType.CAVALRY, "Green Standard Cavalry", 2, 1, 4, 4, 4, 4),
	REFORMED_MUGHAL_MANSABDAR(14, Technology.Group.INDIAN, UnitType.CAVALRY, "Dai-Phat Cavalry", 2, 0, 2, 3, 3, 2),
	REFORMED_MUGHAL_MUSKETEER(12, Technology.Group.INDIAN, UnitType.INFANTRY, "South Indian Infantry", 2, 1, 2, 1, 3, 2),
	REFORMED_STEPPE_RIFLES(30, Technology.Group.NOMAD, UnitType.INFANTRY, "Reformed Steppe Rifles", 3, 3, 3, 3, 4, 4),
	REFORMED_WESTERNIZED_INCAN(30, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Reformed Westernized Incan Infantry", 4, 4, 3, 3, 3, 3),
	ROYAL_MORTAR(25, Technology.Group.WESTERN, UnitType.ARTILLERY, "Royal Mortar", 4, 4, 1, 1, 3, 3),
	RUSSIAN_COSSACK(28, Technology.Group.EASTERN, UnitType.CAVALRY, "Advanced Cossack Cavalry", 2, 1, 4, 4, 5, 4),
	RUSSIAN_CUIRASSIER(28, Technology.Group.EASTERN, UnitType.CAVALRY, "Eastern Cuirassiers", 1, 2, 5, 4, 4, 4),
	RUSSIAN_GREEN_COAT(26, Technology.Group.EASTERN, UnitType.INFANTRY, "Green Coat Infantry", 3, 3, 3, 3, 4, 4),
	RUSSIAN_LANCER(26, Technology.Group.EASTERN, UnitType.CAVALRY, "Lancers", 0, 1, 5, 4, 4, 4),
	RUSSIAN_MASS(30, Technology.Group.EASTERN, UnitType.INFANTRY, "Mass Infantry", 4, 3, 4, 3, 4, 3),
	RUSSIAN_PETRINE(23, Technology.Group.EASTERN, UnitType.INFANTRY, "Petrine Infantry", 2, 3, 2, 3, 3, 3),
	SAXON_INFANTRY(19, Technology.Group.EASTERN, UnitType.INFANTRY, "Saxon Infantry", 3, 2, 3, 2, 2, 2),
	SCHWARZE_REITER(10, Technology.Group.WESTERN, UnitType.CAVALRY, "Schwarze Reiter", 1, 0, 2, 1, 2, 2),
	SCOTTISH_HIGHLANDER(19, Technology.Group.WESTERN, UnitType.INFANTRY, "Highlanders Infantry", 2, 2, 3, 2, 4, 2),
	SHAYBANI(6, Technology.Group.MUSLIM, UnitType.CAVALRY, "Shaybanid Cavalry", 0, 0, 2, 1, 1, 2),
	SIKH_HIT_AND_RUN(18, Technology.Group.INDIAN, UnitType.INFANTRY, "North Indian Musket Infantry", 3, 2, 2, 2, 3, 2),
	SIKH_RIFLE(28, Technology.Group.INDIAN, UnitType.CAVALRY, "Mysorean Light Cavalry", 2, 1, 4, 4, 3, 4),
	SIOUX_DRAGOON(23, Technology.Group.NORTH_AMERICAN, UnitType.CAVALRY, "North American Dragoon", 2, 2, 3, 3, 4, 4),
	SLAVIC_STRADIOTI(6, Technology.Group.EASTERN, UnitType.CAVALRY, "Stratioti Cavalry", 0, 0, 2, 1, 1, 1),
	SMALL_CAST_IRON_BOMBARD(13, Technology.Group.WESTERN, UnitType.ARTILLERY, "Small Cast Iron Cannon", 1, 1, 0, 1, 2, 1),
	SONGHAI_TRIBAL_WARFARE(5, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "West African Warrior", 0, 0, 1, 2, 1, 2),
	SOUTH_AMERICAN_ARQUEBUSIER(10, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Arquebusier", 1, 2, 1, 1, 2, 1),
	SOUTH_AMERICAN_DRAGOON(23, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "South American Dragoon", 2, 2, 3, 3, 4, 4),
	SOUTH_AMERICAN_FOREST_WARFARE(5, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Forest Warriors", 0, 0, 1, 2, 2, 1),
	SOUTH_AMERICAN_GUNPOWDER_WARFARE(10, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Defensive American Musketeers", 1, 1, 1, 2, 1, 2),
	SOUTH_AMERICAN_HORSEMEN(6, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "South American Horsemen", 0, 0, 2, 1, 1, 1),
	SOUTH_AMERICAN_HUSSAR(14, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "South American Hussar", 1, 1, 3, 2, 3, 2),
	SOUTH_AMERICAN_REFORMED_GUNPOWDER_WARFARE(14, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Reformed American Musketeers", 2, 2, 2, 2, 2, 2),
	SOUTH_AMERICAN_RIFLE_CAVALRY(10, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "South American Cavalry", 0, 1, 3, 2, 2, 2),
	SOUTH_AMERICAN_SPEARMEN(1, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "South American Spearmen", 0, 0, 1, 1, 1, 0),
	SOUTH_AMERICAN_SWARM(19, Technology.Group.SOUTH_AMERICAN, UnitType.CAVALRY, "South American Swarm Cavalry", 1, 2, 3, 3, 3, 2),
	SOUTH_AMERICAN_WARFARE(1, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "American Mountain Warriors", 0, 0, 0, 1, 1, 1),
	SOUTH_INDIAN_MUSKETEER(9, Technology.Group.INDIAN, UnitType.INFANTRY, "Poligar Infantry", 1, 2, 1, 1, 1, 1),
	SPANISH_TERCIO(12, Technology.Group.WESTERN, UnitType.INFANTRY, "Tercio Infantry", 1, 2, 1, 2, 2, 2),
	STEPPE_CAVALRY(23, Technology.Group.NOMAD, UnitType.CAVALRY, "Steppe Cavalry", 1, 1, 4, 3, 3, 3),
	STEPPE_FOOTMEN(12, Technology.Group.NOMAD, UnitType.INFANTRY, "Steppe Footmen", 1, 1, 2, 2, 2, 2),
	STEPPE_INFANTRY(19, Technology.Group.NOMAD, UnitType.INFANTRY, "Steppe Infantry", 2, 2, 3, 3, 3, 2),
	STEPPE_LANCERS(14, Technology.Group.NOMAD, UnitType.CAVALRY, "Steppe Lancers", 0, 0, 3, 3, 3, 3),
	STEPPE_MOUNTED_RAIDERS(18, Technology.Group.NOMAD, UnitType.CAVALRY, "Mounted Steppe Raiders", 0, 1, 3, 3, 3, 3),
	STEPPE_MUSKETEERS(15, Technology.Group.NOMAD, UnitType.INFANTRY, "Steppe Musketeers", 2, 1, 3, 2, 2, 2),
	STEPPE_RAIDERS(9, Technology.Group.NOMAD, UnitType.INFANTRY, "Steppe Raiders", 0, 0, 2, 1, 1, 2),
	STEPPE_RIDERS(10, Technology.Group.NOMAD, UnitType.CAVALRY, "Steppe Riders", 0, 0, 3, 2, 3, 2),
	STEPPE_RIFLES(26, Technology.Group.NOMAD, UnitType.INFANTRY, "Steppe Rifles", 2, 3, 3, 3, 4, 4),
	STEPPE_UHLANS(28, Technology.Group.NOMAD, UnitType.CAVALRY, "Steppe Uhlans", 1, 1, 5, 4, 4, 3),
	SWEDISH_ARME_BLANCHE(23, Technology.Group.WESTERN, UnitType.CAVALRY, "Arme Blanche Cavalry", 1, 1, 5, 3, 5, 3),
	SWEDISH_CAROLINE(23, Technology.Group.WESTERN, UnitType.INFANTRY, "Caroline Infantry", 3, 2, 3, 2, 3, 3),
	SWEDISH_GALLOP(18, Technology.Group.WESTERN, UnitType.CAVALRY, "Gallop Cavalry", 1, 1, 4, 3, 3, 3),
	SWEDISH_GUSTAVIAN(19, Technology.Group.WESTERN, UnitType.INFANTRY, "Gustavian Infantry", 3, 2, 3, 2, 3, 2),
	SWISS_LANDSKNECHTEN(9, Technology.Group.WESTERN, UnitType.INFANTRY, "Landsknechten Infantry", 0, 0, 1, 1, 1, 2),
	SWIVEL_CANNON(20, Technology.Group.WESTERN, UnitType.ARTILLERY, "Swivel Cannon", 3, 2, 1, 1, 3, 2),
	TARTAR_COSSACK(26, Technology.Group.EASTERN, UnitType.CAVALRY, "Reformed Cossack Cavalry", 1, 1, 4, 4, 4, 4),
	TIPU_SULTAN_ROCKET(30, Technology.Group.INDIAN, UnitType.INFANTRY, "Indian Drill Infantry", 4, 3, 3, 3, 4, 3),
	TOFONGCHIS_MUSKETEER(12, Technology.Group.MUSLIM, UnitType.INFANTRY, "Muslim Musketeer", 2, 1, 1, 2, 2, 3),
	TOPCHIS_ARTILLERY(14, Technology.Group.MUSLIM, UnitType.CAVALRY, "Muslim Musketeer Cavalry", 1, 0, 2, 2, 3, 2),
	WESTERN_LONGBOW(5, Technology.Group.WESTERN, UnitType.INFANTRY, "Longbow", 0, 0, 1, 0, 1, 1),
	WESTERN_MEDIEVAL_INFANTRY(1, Technology.Group.WESTERN, UnitType.INFANTRY, "Latin Medieval Infantry", 0, 0, 0, 0, 1, 1),
	WESTERN_MEDIEVAL_KNIGHTS(1, Technology.Group.WESTERN, UnitType.CAVALRY, "Latin Knights", 0, 0, 1, 0, 1, 1),
	WESTERN_MEN_AT_ARMS(5, Technology.Group.WESTERN, UnitType.INFANTRY, "Men at Arms", 0, 0, 0, 1, 1, 1),
	WESTERNIZED_ADAL(26, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Westernized East African Infantry", 4, 4, 2, 2, 3, 4),
	WESTERNIZED_AZTEC(26, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Westernized American Hill Infantry", 3, 4, 2, 3, 3, 4),
	WESTERNIZED_BANTU(26, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Westernized South African Infantry", 4, 3, 3, 3, 3, 3),
	WESTERNIZED_ETHIOPIAN(26, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Westernized North African Infantry", 3, 4, 2, 4, 3, 3),
	WESTERNIZED_INCAN(26, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Westernized Incan Infantry", 4, 3, 3, 3, 3, 3),
	WESTERNIZED_MAYAN(30, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Westernized Central American infantry", 4, 3, 3, 3, 3, 4),
	WESTERNIZED_NIGER_KONGOLESE(26, Technology.Group.SUB_SAHARAN, UnitType.INFANTRY, "Westernized Central African Infantry", 3, 3, 3, 3, 4, 3),
	WESTERNIZED_SOUTH_AMERICAN(26, Technology.Group.SOUTH_AMERICAN, UnitType.INFANTRY, "Westernized South American Infantry", 3, 4, 2, 3, 3, 4),
	WESTERNIZED_ZAPOTEC(26, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Westernized American Plains Infantry", 4, 3, 4, 3, 3, 2),
	ZAPOROGHIAN_COSSACK(14, Technology.Group.EASTERN, UnitType.CAVALRY, "Southern Cossacks", 0, 1, 4, 2, 2, 3),
	ZAPOTEC_GUNPOWDER_WARFARE(14, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Offensive American Musketeers", 2, 1, 1, 2, 3, 3),
	ZAPOTEC_PLAINS_WARFARE(10, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "Reformed American Plains Warriors", 0, 0, 1, 2, 3, 2),
	ZAPOTEC_TRIBAL_WARFARE(5, Technology.Group.MESOAMERICAN, UnitType.INFANTRY, "American Plains Warriors", 0, 0, 1, 1, 2, 2),
	
	/*== Samurai Combat Simulator ==*/
	
	// Samurai Infantry
	YARI_ASHIGARU(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Yari Ashigaru", 3,1,2,2,5,2),
	BOW_ASHIGARU(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Bow Ashigaru", 2,1,4,2,3,1),
	FIRE_ROCKETS(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Fire Rockets", 2,2,10,4,2,1),
	BOMB_THROWERS(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Bomb Throwers", 3,1,4,1,2,1),
	HAND_CANNONS(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Hand Cannons", 3,1,4,1,2,1),
	MATCHLOCK_ASHIGARU(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Matchlock Ashigaru", 2,2,4,2,3,1),
	NINJA(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Ninja", 14,11,20,10,12,16),
	YARI_JI_SAMURAI(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Yari Ji-Samurai", 6,8,15,7,10,5),
	BOW_JI_SAMURAI(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Bow Ji-Samurai", 6,6,4,2,8,4),
	MATCHLOCK_JI_SAMURAI(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Mathlock Ji-Samurai", 3,3,3,1,3,1),
	YARI_RONIN(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Yari Ronin", 6,8,15,7,10,5),
	BOW_RONIN(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Bow Ronin", 2,1,4,2,3,1),
	MATCHLOCK_RONIN(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Matchlock Ronin", 2,1,4,2,3,1),
	SWORD_RONIN(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Sword Ronin", 11,4,12,5,10,5),
	NAGINATA_RONIN(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Naginata Ronin", 12,5,14,7,20,10),
	YARI_MONKS(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Yari Monks", 8,4,15,5,10,6),
	BOW_MONKS(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Bow Monks", 7,8,18,4,8,8),
	MATCHLOCK_MONKS(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Matchlock Monks", 7,3,18,2,8,8),
	NAGINATA_MONKS_RONIN(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Naginata Monks Ronin", 12,7,12,6,15,8),
	BANDITS(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Bandits", 10,5,6,3,12,6),
	WOKOU_PIRATES(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Wokou Pirates", 12,7,12,6,15,8),
	YARI_EIYUU(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Yari Eiyuu", 7,9,15,7,11,6),
	BOW_EIYUU(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Bow Eiyuu", 8,6,10,5,10,5),
	MATCHLOCK_EIYUU(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Matchlock Eiyuu", 8,0,8,2,2,2),
	SWORD_EIYUU(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Sword Eiyuu", 25,10,28,14,40,20),
	NAGINATA_EIYUU(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Naginata Eiyuu", 22,18,22,11,45,6),
	ONNA_BUSHI(1, Technology.Group.SAMURAI, UnitType.INFANTRY,
			"Onna Bushi (Naginata)", 12,7,16,8,12,12),
	
	// Samurai Cavalry
	MOUNTED_BOW_EIYUU(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"Eiyuu Mounted Bows", 7,8,18,4,8,8),
	MOUNTED_YARI_EIYUU(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"Eiyuu Mounted Yari", 7,8,21,3,9,9),
	MOUNTED_KATANA_EIYUU(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"Mounted Katana Eiyuu", 18,7,15,7,14,7),
	MOUNTED_GUNNER_EIYUU(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"Eiyuu Mounted Gunners", 7,7,3,2,7,7),
	MOUNTED_ONNA_BUSHI(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"Mounted Onna Bushi (Nagitana)", 12,8,12,8,8,8),
	GENERAL_HATAMOTO(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"General's Hatamoto", 17,17,17,17,18,18),
	MOUNTED_ASHIGARU(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"Mounted Ashigaru", 4,2,20,3,8,4),
	MOUNTED_BOW_SAMURAI(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"Mounted Bow Samurai", 6,6,6,3,10,5),
	MOUNTED_YARI_SAMURAI(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"Mounted Yari Samurai", 6,6,6,3,10,5),
	MOUNTED_GUNNER(1, Technology.Group.SAMURAI, UnitType.CAVALRY,
			"Mounted Gunners", 5,6,5,2,10,5),
	
	// Samurai Artillery
	TREBUCHET(1, Technology.Group.SAMURAI, UnitType.ARTILLERY,
			"Mangonels / Trebuchets", 2,2,3,4,2,2),
	CHINESE_IRON_CANNON(1, Technology.Group.SAMURAI, UnitType.ARTILLERY,
			"Chinese Iron Cannons", 2,2,10,4,2,1),
	JAPANESE_BRONZE_CANNON(1, Technology.Group.SAMURAI, UnitType.ARTILLERY,
			"Japanese Bronze Cannon", 4,2,11,4,2,1),
	PROVINCE_DESTROYER(1, Technology.Group.SAMURAI, UnitType.ARTILLERY,
			"Destroyer of Provinces", 4,2,20,3,4,2)
	;
	/**
	 * Military technology level the unit becomes available (in the range of 0-32)
	 */
	public final int tech_level;
	/**
	 * {@link TechnologyGroup} of the Unit. All artillery will be considered WESTERN tech by convention (it is available
	 * to all tech groups).
	 */
	public final Technology.Group tech_group;
	/** Type of the Unit, one of INFANTRY, CAVALRY, ARTILLERY */
	public final UnitType type;
	/** Name of the unit, such as "Western Longbow" */
	public final String name;
	/** Offensive fire pips (0-6) */
	public final int off_fire;
	/** Defensive fire pips (0-6) */
	public final int def_fire;
	/** Offensive shock pips (0-6) */
	public final int off_shock;
	/** Defensive shock pips (0-6) */
	public final int def_shock;
	/** Offensive morale pips (0-6) */
	public final int off_morale;
	/** Defensive morale pips (0-6) */
	public final int def_morale;
	
	private Unit(int tech_level, Technology.Group techgroup, UnitType type, String name, int off_fire, int def_fire,
			int off_shock, int def_shock, int off_morale, int def_morale) {
		this.tech_level = tech_level;
		this.tech_group = techgroup;
		this.type = type;
		this.name = name;
		this.off_fire = off_fire;
		this.def_fire = def_fire;
		this.off_shock = off_shock;
		this.def_shock = def_shock;
		this.off_morale = off_morale;
		this.def_morale = def_morale;
	}
	
	/**
	 * @return Pips of the unit for the given phase (FIRE/SHOCK/MORALE) and combat type (OFFENSE/DEFENSE)
	 */
	public int getPips(Phase phase, CombatType combatType) {
		if (combatType == CombatType.OFFENSE && phase == Phase.FIRE) { return off_fire; }
		if (combatType == CombatType.DEFENSE && phase == Phase.FIRE) { return def_fire; }
		if (combatType == CombatType.OFFENSE && phase == Phase.SHOCK) { return off_shock; }
		if (combatType == CombatType.DEFENSE && phase == Phase.SHOCK) { return def_shock; }
		if (combatType == CombatType.OFFENSE && phase == Phase.FIRE) { return off_fire; }
		if (combatType == CombatType.DEFENSE && phase == Phase.FIRE) { return def_fire; }
		if (combatType == CombatType.OFFENSE && phase == Phase.MORALE) { return off_morale; }
		if (combatType == CombatType.DEFENSE && phase == Phase.MORALE) { return def_morale; }
		return 0;
	}
	
	/**
	 * Convenience method to find a Unit of the given name. If no unit of the exact same name exists, <code>null</code>
	 * is returned
	 */
	public static Unit fromString(String name) {
		return Stream.of(Unit.values()).filter(u -> u.name.equalsIgnoreCase(name)).findAny().orElse(null);
	}
	
	/**
	 * @return The {@link #name} (don't confuse with {@link #name()} from {@link Enum}!
	 */
	public String getName() {
		return name;
	}
	
	public String getTooltip() {
		StringBuilder sb = new StringBuilder();
		// sb.append("(Pips: (off. fire,def. fire,off. shock, def. shock, off.
		// morale, def. morale): \n");
		sb.append("Off. Fire: \t" + off_fire + "\n");
		sb.append("Def. Fire: \t" + def_fire + "\n");
		sb.append("Off. Shock: \t" + off_shock + "\n");
		sb.append("Def. Shock: \t" + def_shock + "\n");
		sb.append("Off. Morale: \t" + off_morale + "\n");
		sb.append("Def. Morale: \t" + def_morale + "\n");
		sb.append("Total Pips: \t"
				+ Integer.toString(off_fire + def_fire + off_shock + def_shock + off_morale + def_morale) + "\n");
		
		return sb.toString();
	}
}
package eu4.comsim.core.datatypes;

import java.util.stream.Stream;

/**
 * Enumeration of all Terrain types in the game.<br>
 *
 * @see <a href="http://www.eu4wiki.com/Land_warfare#Terrain">http://www.eu4wiki .com/Land_warfare#Terrain</a>
 */
public enum Terrain {
	
	GLACIAL("Glacial", 4, 1.0, 1.0, 1.0, 1, "/terrain/Combat_terrain_arctic.png"),
	FARMLANDS("Farmlands", 10, 1.0, 1.1, 1.0, 0, "/terrain/Combat_terrain_farmlands.png"),
	FOREST("Forest", 6, 1.0, 1.25, 0.8, 1, "/terrain/Combat_terrain_forest.png"),
	HILLS("Hills", 4, 1.1, 1.4, 0.75, 1, "/terrain/Combat_terrain_hills.png"),
	WOODS("Woods", 6, 1.0, 1.1, 0.8, 1, "/terrain/Combat_terrain_woods.png"),
	MOUNTAINS("Mountains", 4, 1.25, 1.5, 0.5, 2, "/terrain/Combat_terrain_mountains.png"),
	GRASSLANDS("Grasslands", 8, 1.0, 1.0, 1.0, 0, "/terrain/Combat_terrain_grasslands.png"),
	JUNGLE("Jungle", 4, 1.0, 1.5, 0.75, 1, "/terrain/Combat_terrain_jungle.png"),
	MARSH("Marsh", 4, 1.0, 1.3, 0.75, 1, "/terrain/Combat_terrain_marsh.png"),
	DESERT("Desert", 4, 1.0, 1.05, 1.0, 0, "/terrain/Combat_terrain_desert.png"),
	COASTAL_DESERT("Coastal Desert", 4, 1.0, 1.0, 1.0, 0, "/terrain/Combat_terrain_coastal_desert.png"),
	COASTLINE("Coastline", 6, 1.0, 1.0, 1.0, 0, "/terrain/Combat_terrain_inland_ocean.png"),
	DRYLANDS("Drylands", 6, 1.0, 1.0, 1.0, 0, "/terrain/Combat_terrain_drylands.png"),
	HIGHLANDS("Highlands", 6, 1.1, 1.4, 0.75, 1, "/terrain/Combat_terrain_highlands.png"),
	SAVANNAH("Savannah", 4, 1.0, 1.0, 1.0, 0, "/terrain/Combat_terrain_savannah.png"),
	STEPPE("Steppe", 6, 1.0, 1.0, 1.0, 0, "/terrain/Combat_terrain_steppe.png");
	
	/** Human-readable name of the terrain */
	public final String name;
	/**
	 * Modifier for base supply limit from terrain. Not directly used in combat
	 */
	public final int supplyLimit;
	/** determines the length of siege phases */
	public final double localDefensiveness;
	/** determines travel time of units */
	public final double movementCost;
	/**
	 * how many armies can be in the front and back row next to each other ( "length of battle line")
	 */
	public final double combatWidth;
	/** Flat modifier to attacker's die rolls (positive) */
	public final int attackerPenalty;
	/** Classpath-relative image */
	public final String imagePath;
	
	private Terrain(String name, int supplyLimit, double localDefensiveness, double movementCost, double combatWidth,
			int attackerPenalty, String imagePath) {
		this.name = name;
		this.supplyLimit = supplyLimit;
		this.localDefensiveness = localDefensiveness;
		this.movementCost = movementCost;
		this.combatWidth = combatWidth;
		this.attackerPenalty = attackerPenalty;
		this.imagePath = imagePath;
	}
	
	/**
	 * Convenience method to find a Terrain of the given name. If no Terrain of the exact same name exists,
	 * <code>null</code> is returned
	 */
	public static Terrain fromString(String name) {
		return Stream.of(Terrain.values()).filter(t -> t.name.equalsIgnoreCase(name)).findAny().orElse(null);
	}
	
	public enum CrossingPenalty {
		NONE("No crossing penalty", 0),
		RIVER("Crossing river", 1),
		STRAIT("Crossing strait", 2),
		AMPHIBIOUS_LANDING("Amphibious landing", 2);
		
		/** Human-readable name of the penalty */
		public final String name;
		/** Penalty value as a non-negative integer */
		public final int penalty;
		
		private CrossingPenalty(String name, int penaltyToRoll) {
			this.name = name;
			this.penalty = penaltyToRoll;
		}
		
	}
}

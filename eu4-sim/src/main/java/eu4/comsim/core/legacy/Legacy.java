package eu4.comsim.core.legacy;

import eu4.comsim.core.General;

/**
 * Move some code here that is not used anymore/at the moment, but might be later
 */
public class Legacy {
	
	public static General generate(int bonusFire, int bonusShock, int bonusManeuver, int bonusSiege,
			double armyTradition) {
		int fire = bonusFire;
		int shock = bonusShock;
		int maneuver = bonusManeuver;
		int siege = bonusSiege;
		// calculate pipPool
		int pipPool;
		pipPool = java.util.concurrent.ThreadLocalRandom.current().nextInt(6) + 1;
		pipPool += armyTradition / 20.0;
		for (double tempTradition = armyTradition; tempTradition > 0; tempTradition -= 20) {
			if (java.util.concurrent.ThreadLocalRandom.current().nextInt(100) < tempTradition) {
				pipPool++;
			}
		}
		if (java.util.concurrent.ThreadLocalRandom.current().nextInt(100) < 50) {
			pipPool++;
		}
		
		// distribute pipPool
		while (pipPool > 10) {
			fire++;
			shock++;
			maneuver++;
			siege++;
			pipPool -= 4;
		}
		int increaseTarget;
		while (pipPool > 0) {
			increaseTarget = java.util.concurrent.ThreadLocalRandom.current().nextInt(10);
			if (increaseTarget == 0 && siege < 6) {
				siege++;
				pipPool--;
			} else if (increaseTarget < 4 && shock < 6) {
				shock++;
				pipPool--;
			} else if (increaseTarget < 7 && fire < 6) {
				fire++;
				pipPool--;
			} else if (maneuver < 6) {
				maneuver++;
				pipPool--;
			}
		}
		return new General(fire, shock, maneuver, siege);
	}
	
}

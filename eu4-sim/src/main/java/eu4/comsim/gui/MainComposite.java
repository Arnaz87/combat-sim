package eu4.comsim.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.NumberFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.Army;
import eu4.comsim.core.Battle;
import eu4.comsim.core.BattleStats;
import eu4.comsim.core.SimulationController;
import eu4.comsim.core.Util;
import eu4.comsim.core.datatypes.Terrain;
import eu4.comsim.core.datatypes.Terrain.CrossingPenalty;

/**
 * Main composite of the GUI
 */
public class MainComposite extends Composite {
	
	public static final File PERSIST_FILE = new File("lastRun.cs");
	
	private static final String FONT = "Segoe UI";
	private static final int MIN_HANDICAP = -3;
	private static final int MAX_HANDICAP = 3;
	private static final String TOOLTIP_HANDICAP = "Select a handicap for the attacking army (+3 to -3). Value will be added (up to a maximum of 9, down to a minimum of 0) to each single random die roll.";
	
	Spinner spinner_numberOfRuns;
	BattleComposite battleComposite;
	ArmyGroup attackerGroup;
	ArmyGroup defenderGroup;
	TerrainGroup trngrpTerrain;
	ScrolledComposite attackerScroll;
	
	public MainComposite(Composite parent, int style) {
		super(parent, style);
		GridLayout layout_main = new GridLayout(3, false);
		setLayout(layout_main);
		
		getShell().addShellListener(new ShellAdapter() {
			@Override
			public void shellClosed(ShellEvent e) {
				try (FileOutputStream fos = new FileOutputStream(PERSIST_FILE);
						ObjectOutputStream oos = new ObjectOutputStream(fos)) {
					
					oos.writeObject(attackerGroup.buildArmy());
					oos.writeObject(defenderGroup.buildArmy());
					oos.writeObject(trngrpTerrain.selectedTerrain);
					oos.writeObject(trngrpTerrain.getPenalty());
					oos.close();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		});
		
		Battle b = getInitialBattle();
		
		attackerGroup = new ArmyGroup(this, SWT.NONE, "Attacker", b.armyA);
		attackerGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		
		trngrpTerrain = new TerrainGroup(this, SWT.NONE, b.terrain, b.crossingPenalty);
		GridData gd_trngrpTerrain = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_trngrpTerrain.widthHint = 250;
		gd_trngrpTerrain.heightHint = 100;
		trngrpTerrain.setLayoutData(gd_trngrpTerrain);
		
		defenderGroup = new ArmyGroup(this, SWT.NONE, "Defender", b.armyB);
		defenderGroup.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 2));
		
		Composite composite_simulation = new Composite(this, SWT.NONE);
		GridData gd_composite_simulation = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_composite_simulation.widthHint = 250;
		composite_simulation.setLayoutData(gd_composite_simulation);
		composite_simulation.setLayout(new GridLayout(2, true));
		
		Group group_luck = new Group(composite_simulation, SWT.NONE);
		GridData gd_group_luck = new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1);
		gd_group_luck.widthHint = 150;
		group_luck.setLayoutData(gd_group_luck);
		GridLayout gl_group_luck = new GridLayout(2, true);
		group_luck.setLayout(gl_group_luck);
		
		Label lblRollHandicap = new Label(group_luck, SWT.NONE);
		lblRollHandicap.setAlignment(SWT.CENTER);
		lblRollHandicap.setLayoutData(new GridData(SWT.CENTER, SWT.TOP, true, false, 2, 1));
		lblRollHandicap.setText("Roll Handicap");
		
		Spinner combo_attackerRoll = new Spinner(group_luck, SWT.BORDER);
		combo_attackerRoll.setToolTipText(TOOLTIP_HANDICAP);
		combo_attackerRoll.setMaximum(MAX_HANDICAP);
		combo_attackerRoll.setMinimum(MIN_HANDICAP);
		
		Spinner combo_defenderRoll = new Spinner(group_luck, SWT.BORDER);
		combo_defenderRoll.setToolTipText(TOOLTIP_HANDICAP);
		combo_defenderRoll.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		combo_defenderRoll.setMaximum(MAX_HANDICAP);
		combo_defenderRoll.setMinimum(MIN_HANDICAP);
		
		Button btnSimulate = new Button(composite_simulation, SWT.NONE);
		btnSimulate.setToolTipText(
				"Run the simulation. The same battle will be initialized the number of times specified by the \"Runs\" setting. Results will display average values over this sample size.");
		btnSimulate.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		btnSimulate.setFont(SWTResourceManager.getFont(FONT, 10, SWT.BOLD));
		btnSimulate.setImage(SWTResourceManager.getImage(MainComposite.class, "/general/Prestige_from_land.png"));
		btnSimulate.setBackgroundImage(null);
		btnSimulate.setText("Simulate");
		
		ProgressBar progressBar = new ProgressBar(composite_simulation, SWT.SMOOTH);
		progressBar.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		progressBar.setMaximum(1000);
		
		Label lblRuns = new Label(composite_simulation, SWT.NONE);
		lblRuns.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		lblRuns.setText("Samples:");
		lblRuns.setToolTipText(
				"How many times the battle will be simulated, minimizing the effect of the dice roll randomness. More iterations produce a more accurate result, but take longer");
		
		spinner_numberOfRuns = new Spinner(composite_simulation, SWT.BORDER);
		spinner_numberOfRuns.setToolTipText("Sample size (1-1000)");
		spinner_numberOfRuns.setSelection(100);
		spinner_numberOfRuns.setMinimum(1);
		spinner_numberOfRuns.setMaximum(1000);
		spinner_numberOfRuns.setIncrement(50);
		
		battleComposite = new BattleComposite(this, SWT.BORDER);
		battleComposite.setBackgroundMode(SWT.INHERIT_DEFAULT);
		battleComposite.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		GridData gd_battleComposite = new GridData(SWT.CENTER, SWT.CENTER, false, false, 3, 1);
		gd_battleComposite.widthHint = 900;
		battleComposite.setLayoutData(gd_battleComposite);
		
		updateBattlefield();
		
		Label label_resultseparator = new Label(this, SWT.BORDER | SWT.SEPARATOR | SWT.HORIZONTAL | SWT.SHADOW_NONE);
		label_resultseparator.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_NORMAL_SHADOW));
		label_resultseparator.setFont(SWTResourceManager.getFont(FONT, 20, SWT.BOLD));
		label_resultseparator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 3, 1));
		
		ResultsComposite results_attacker = new ResultsComposite(this, SWT.NONE);
		results_attacker.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		
		Composite composite_overallResults = new Composite(this, SWT.NONE);
		composite_overallResults.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1));
		composite_overallResults.setLayout(new GridLayout(2, true));
		
		Label label = new Label(composite_overallResults, SWT.NONE);
		label.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 2, 1));
		label.setText("Avg. rolls");
		label.setAlignment(SWT.CENTER);
		
		Label label_averageRollA = createRollLabel(composite_overallResults, "attacker");
		Label label_averageRollB = createRollLabel(composite_overallResults, "defender");
		new Label(composite_overallResults, SWT.NONE);
		
		Label label_battleDuration = new Label(composite_overallResults, SWT.CENTER);
		GridData gd_label_battleDuration = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_label_battleDuration.verticalIndent = 20;
		label_battleDuration.setLayoutData(gd_label_battleDuration);
		label_battleDuration.setFont(SWTResourceManager.getFont(FONT, 12, SWT.BOLD));
		label_battleDuration.setAlignment(SWT.CENTER);
		// Preset some invisible text so layout does not set get screwed
		label_battleDuration.setText("50000 days\r\n");
		label_battleDuration.setVisible(false);
		
		Label label_casualties = new Label(composite_overallResults, SWT.CENTER);
		label_casualties.setFont(SWTResourceManager.getFont(FONT, 12, SWT.BOLD));
		GridData gd_label_casualties = new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1);
		gd_label_casualties.verticalIndent = 20;
		label_casualties.setLayoutData(gd_label_casualties);
		label_casualties.setText("20000 dead");
		label_casualties.setVisible(false);
		
		ResultsComposite results_defender = new ResultsComposite(this, SWT.NONE);
		results_defender.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		
		btnSimulate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				int numberOfIterations = spinner_numberOfRuns.getSelection();
				Army armyA = attackerGroup.buildArmy();
				Army armyB = defenderGroup.buildArmy();
				battleComposite.update(createBattle());
				SimulationController sim = new SimulationController(numberOfIterations, armyA, armyB,
						trngrpTerrain.getTerrain(), trngrpTerrain.getPenalty(), progressBar);
				sim.run();
				List<BattleStats> statsA = sim.getResults(armyA);
				List<BattleStats> statsB = sim.getResults(armyB);
				results_attacker.update(statsA);
				results_defender.update(statsB);
				NumberFormat df = NumberFormat.getInstance();
				df.setMaximumFractionDigits(2);
				label_averageRollA.setText(df.format(armyA.dieRollProvider.averageRoll()));
				label_averageRollA.setVisible(true);
				label_averageRollB.setText(df.format(armyB.dieRollProvider.averageRoll()));
				label_averageRollB.setVisible(true);
				double totalCasualties = statsA.stream().collect(Collectors.averagingInt(BattleStats::casualties))
						+ statsB.stream().collect(Collectors.averagingInt(BattleStats::casualties));
				double averageDuration = statsA.stream().collect(Collectors.averagingInt(BattleStats::getDuration));
				
				label_battleDuration.setText(Util.IF.format(averageDuration) + " days");
				label_battleDuration.setVisible(true);
				label_casualties.setText(Util.IF.format(totalCasualties) + " dead");
				label_casualties.setVisible(true);
				
			}
		});
		
	}
	
	private static Label createRollLabel(Composite parent, String who) {
		Label label_averageRollA = new Label(parent, SWT.BORDER);
		label_averageRollA.setAlignment(SWT.CENTER);
		GridData gd_label_averageRollA = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_label_averageRollA.minimumWidth = 20;
		label_averageRollA.setLayoutData(gd_label_averageRollA);
		label_averageRollA.setToolTipText("Average " + who + " roll (includes handicap!).");
		label_averageRollA.setText("buuuuuh");
		label_averageRollA.setVisible(false);
		return label_averageRollA;
	}
	
	public void updateBattlefield() {
		try {
			battleComposite.update(createBattle());
		} catch (Exception e) {
			// Might fail during initialization due to order, little bit
			// unelegant but IGNORE
		}
	}
	
	public Battle createBattle() {
		Army armyA = attackerGroup.buildArmy();
		Army armyB = defenderGroup.buildArmy();
		return new Battle(armyA, armyB, trngrpTerrain.getTerrain(), trngrpTerrain.getPenalty());
	}
	
	private static Battle getInitialBattle() {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(PERSIST_FILE));) {
			Army armyA = (Army) ois.readObject();
			Army armyB = (Army) ois.readObject();
			Terrain terrain = (Terrain) ois.readObject();
			CrossingPenalty cp = (CrossingPenalty) ois.readObject();
			
			return new Battle(armyA, armyB, terrain, cp);
		} catch (IOException | ClassNotFoundException e) {
			System.err.println("Warning: Could not read previous session, ignore if first run. Using default settings");
			System.err.println(e.toString());
		}
		return new Battle(Army.TEMPLATE, Army.TEMPLATE, Terrain.FOREST, CrossingPenalty.NONE);
	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
}

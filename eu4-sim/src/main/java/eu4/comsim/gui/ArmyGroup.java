package eu4.comsim.gui;

import java.util.Collection;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.Army;
import eu4.comsim.core.DieRollProvider;
import eu4.comsim.core.Regiment;
import eu4.comsim.core.Technology;
import eu4.comsim.core.datatypes.Unit;
import eu4.comsim.core.datatypes.UnitType;

/**
 * Composite containing all information relevant for one side of a battle.<br>
 * A {@link MainComposite} will contain two of these; one for the attacking and one for the defending side
 */
public class ArmyGroup extends Group implements Listener {
	
	ScrolledComposite scroll;
	Composite inner;
	MainComposite mainComposite;
	
	// Comboboxes for Unit - input will automatically be updated when
	// technology group or level is changed. Initialized in constructor
	Combo[] combos_unit = new Combo[3];
	// Controls number of regiments of each Unit
	Spinner[] spinners_unit = new Spinner[3];
	
	// Sub composites - General and Technology
	GeneralGroup group_general;
	TechnologyGroup group_tech;
	
	/**
	 * Create a new {@link ArmyGroup} composite.
	 *
	 * @param name
	 *            The name used as the Group's text
	 */
	public ArmyGroup(Composite parent, int style, String name, Army army) {
		super(parent, style | SWT.V_SCROLL);
		setText(name);
		//setLayout(new GridLayout(1, false));
		setLayout(new FillLayout());
		
		mainComposite = (MainComposite) parent;
		
		// Scrollbar Groups Creation
		scroll = new ScrolledComposite(this, SWT.V_SCROLL | SWT.H_SCROLL);
		scroll.setLayout(new FillLayout());
		inner = new Composite(scroll, SWT.NONE);
		inner.setLayout(new GridLayout(1, false));
		
		group_tech = new TechnologyGroup(inner, SWT.NONE, "Technology", army.technology, this);
		group_tech.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		
		Group grpArmy = new Group(inner, SWT.NONE);
		grpArmy.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpArmy.setText("Army");
		grpArmy.setLayout(new GridLayout(1, false));
		
		group_general = new GeneralGroup(grpArmy, SWT.NONE, "General", army.general);
		group_general.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 1, 1));
		Group grpRegiments = new Group(grpArmy, SWT.NONE);
		grpRegiments.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		grpRegiments.setText("Regiments");
		GridLayout gl_grpRegiments = new GridLayout(3, false);
		grpRegiments.setLayout(gl_grpRegiments);
		
		for (UnitType type : UnitType.values()) {
			Label label_type = new Label(grpRegiments, SWT.NONE);
			label_type.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
			label_type.setToolTipText(type.name);
			label_type.setImage(SWTResourceManager.getImage(MainComposite.class, type.imagePath));
			
			combos_unit[type.id] = new Combo(grpRegiments, SWT.READ_ONLY);
			combos_unit[type.id].setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true, 1, 1));
			combos_unit[type.id].addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					try {
						String unitAsString = ((Combo) e.getSource()).getText();
						Unit unit = Unit.fromString(unitAsString.substring(unitAsString.indexOf(" ") + 1));
						combos_unit[type.id].setToolTipText(unit.getTooltip());
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			});
			
			Spinner spinner_type = new Spinner(grpRegiments, SWT.BORDER);
			spinner_type.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, false, 1, 1));
			spinner_type.setSelection(type.getDefaultBataillonSize());
			spinners_unit[type.id] = spinner_type;
			spinners_unit[type.id].addModifyListener(e -> updateBattlefield());
		}
		

		// Scrollbar Methods
	    scroll.setMinHeight(inner.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
	    scroll.setMinWidth(inner.computeSize(SWT.DEFAULT, SWT.DEFAULT).x);
		scroll.setContent(inner);
		scroll.setExpandHorizontal(true);
		scroll.setExpandVertical(true);
		
		updateCombos();
		// Consider moving that to an initialization routine that is independent
		// of constructor
		fromRegiments(army.regiments);
	}
	
	public void updateBattlefield() {
		mainComposite.updateBattlefield();
	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	/**
	 * @return Collects general and regiments for all the input, and builds an {@link Army} from it.
	 */
	public Army buildArmy() {
		Technology tech = group_tech.getTechnology();
		return new Army(group_general.getGeneral(), tech, getRegiments(), DieRollProvider.random());
	}
	
	/**
	 * Exposes the child control {@link GeneralGroup} to clients
	 *
	 * @return the {@link GeneralGroup}
	 */
	public GeneralGroup getGeneralGroup() {
		return group_general;
	}
	
	/**
	 * The values of all the Unit combo boxes are updated
	 *
	 * Fills a {@link Combo} with {@link Unit}s of the given {@link UnitType} available at the given {@link Technology}
	 * level<br>
	 * The results will be sorted by tech level in descending order, so the most recent units are on top.
	 *
	 */
	public void updateCombos() {
		Technology tech = group_tech.getTechnology();
		for (int i = 0; i < combos_unit.length; i++) {
			UnitType type = UnitType.values()[i];
			if (combos_unit[i] == null) { return; }
			// Predicate: Same UnitType, techlevel of Unit does not exceed mil
			// tech
			// level reached and techgroup must match (for non-artillery)
			Predicate<? super Unit> isSelectable = u -> (u.type == type && u.tech_level <= tech.getLevel().level
					&& ((u.type != UnitType.ARTILLERY) ? u.tech_group == tech.getGroup() : true));
			// Return all units that match the selection criteria, sort them
			// descending by tech level
			Stream<Unit> eligible = Stream.of(Unit.values())
					.filter(isSelectable)
					.sorted((u1, u2) -> Integer.compare(u2.tech_level, u1.tech_level));
			String[] names = eligible.map(u -> "[" + Integer.toString(u.tech_level) + "] " + u.getName())
					.toArray(String[]::new);
			if (names.length > 0) {
				combos_unit[i].setItems(names);
				combos_unit[i].setText(combos_unit[i].getItem(0));
				String unit = combos_unit[i].getText();
				String trimmed = (unit.length() > 0) ? unit.substring(unit.indexOf(" ") + 1) : "No unit selected";
				combos_unit[i].setToolTipText(Unit.fromString(trimmed).getTooltip());
				combos_unit[i].setEnabled(true);
			} else {
				combos_unit[i].setItems(new String[] { "No unit available" });
				combos_unit[i].setText(combos_unit[i].getItem(0));
				combos_unit[i].setToolTipText("No unit available at this tech level");
				combos_unit[i].setEnabled(false);
			}
			
		}
	}
	
	/**
	 * @return Reads the value of the {@link #combos_unit} boxes and creates a new set of regiments from it
	 */
	private List<Regiment> getRegiments() {
		
		@SuppressWarnings("serial")
		Map<Unit, Integer> boilerPlate = new Hashtable<Unit, Integer>() {
			{
				for (int i = 0; i < combos_unit.length; i++) {
					// "[10] Schwarze Reiter" -> "Schwarze Reiter"
					String unit = combos_unit[i].getText();
					String trimmed = unit.substring(unit.indexOf(" ") + 1);
					if (Unit.fromString(trimmed) != null) {
						put(Unit.fromString(trimmed), spinners_unit[i].getSelection());
					}
				}
			}
		};
		return Regiment.createRegiments(group_tech.getTechnology(), boilerPlate);
	}
	
	/**
	 * TODO Merge with updateCombos
	 */
	private void fromRegiments(Collection<Regiment> regs) {
		for (int i = 0; i < UnitType.values().length; i++) {
			Collection<Regiment> contingent = Regiment.getRegiments(regs, UnitType.values()[i]);
			if (contingent.iterator().hasNext()) {
				Unit u = contingent.iterator().next().unit;
				combos_unit[i].setText("[" + Integer.toString(u.tech_level) + "] " + u.getName());
				spinners_unit[i].setSelection(contingent.size());
			} else {
				spinners_unit[i].setSelection(0);
			}
		}
		
	}

	@Override
	public void handleEvent(Event arg0) {
		Point location = this.getLocation();
		//location.y = -vbar.getSelection();
		this.setLocation(location);
	}
	
}

package eu4.comsim.gui;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.Technology;
import eu4.comsim.core.Util;

/**
 * A {@link Group} holding all information regarding a nation/army's {@link Technology}, and providing input controls
 * for the user to change various aspects of it.
 */
public class TechnologyGroup extends Group {
	
	Technology.Group selectedTechGroup;
	
	ArmyGroup armyGroup;
	
	private Spinner spinner_level;
	
	private Spinner spinner_morale;
	private Spinner spinner_disc;
	
	private Spinner spinner_infCA;
	private Spinner spinner_cavCA;
	private Spinner spinner_artCA;
	
	private Label label_totalMorale;
	private Label label_totalTactics;
	private Composite composite;
	
	public TechnologyGroup(Composite parent, int style, String name, Technology tech, ArmyGroup ag) {
		super(parent, style);
		selectedTechGroup = tech.getGroup();
		setLayout(new GridLayout(4, false));
		setText(name);
		
		armyGroup = ag;
		
		Label label_level = new Label(this, SWT.CENTER);
		label_level.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_level.setToolTipText("Military technology level");
		label_level.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Military_tech.png"));
		
		spinner_level = new Spinner(this, SWT.BORDER);
		spinner_level.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		spinner_level.setToolTipText("Select military tech level (1-32)");
		spinner_level.setMaximum(32);
		spinner_level.setMinimum(1);
		spinner_level.setSelection(tech.getLevel().level);
		spinner_level.addModifyListener(e -> {
			label_totalMorale.setText(Util.DF.format(getTechnology().getMorale()));
			label_totalTactics.setText(Util.DF.format(getTechnology().getTactics()));
			armyGroup.updateCombos();
			armyGroup.updateBattlefield();
		});
		Button button_techgroup = new Button(this, SWT.NONE);
		button_techgroup.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		button_techgroup.setToolTipText("Click to choose technology group");
		button_techgroup.setImage(SWTResourceManager.getImage(Technology.Group.class, selectedTechGroup.imagePath));
		
		Menu menu_chooseTG = new Menu(button_techgroup);
		button_techgroup.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Rectangle bounds = button_techgroup.getBounds();
				Point point = button_techgroup.getParent().toDisplay(bounds.x, bounds.y + bounds.height);
				menu_chooseTG.setLocation(point);
				menu_chooseTG.setVisible(true);
			}
		});
		
		for (Technology.Group tg : Technology.Group.values()) {
			MenuItem item = new MenuItem(menu_chooseTG, SWT.RADIO);
			item.setText(tg.name);
			item.setSelection(tg == selectedTechGroup);
			item.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
					selectedTechGroup = Technology.Group.fromString(item.getText());
					button_techgroup
							.setImage(SWTResourceManager.getImage(Technology.Group.class, selectedTechGroup.imagePath));
					// somewhat ugly but whatever
					armyGroup.updateCombos();
					
				}
			});
		}
		@SuppressWarnings("unused")
		Label placeholder = new Label(this, SWT.NONE);
		
		Label separator = new Label(this, SWT.SEPARATOR | SWT.HORIZONTAL);
		separator.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 4, 1));
		
		Label label_morale = new Label(this, SWT.NONE);
		label_morale.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_morale.setToolTipText("Morale");
		label_morale.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Morale.png"));
		
		composite = new Composite(this, SWT.BORDER);
		composite.setLayout(new GridLayout(1, false));
		composite.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		
		spinner_morale = new Spinner(composite, SWT.BORDER);
		spinner_morale.setToolTipText("Select morale multiplier (100 = 100% of base)");
		spinner_morale.setMaximum(500);
		spinner_morale.setSelection((int) (tech.getMoraleModifier() * 100));
		spinner_morale.addModifyListener(e -> {
			label_totalMorale.setText(Util.DF.format(getTechnology().getMorale()));
			armyGroup.updateBattlefield();
		});
		label_totalMorale = new Label(composite, SWT.NONE);
		label_totalMorale.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		label_totalMorale.setToolTipText("Total morale (Base value x modifier)");
		label_totalMorale.setText(Util.DF.format(tech.getMorale()));
		
		Label label_infCA = new Label(this, SWT.NONE);
		label_infCA.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_infCA.setToolTipText("Infantry combat ability");
		label_infCA.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Infantry_power.png"));
		
		spinner_infCA = new Spinner(this, SWT.BORDER);
		spinner_infCA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_infCA.setMaximum(500);
		spinner_infCA.setSelection((int) (tech.getInfCombatAbility() * 100));
		spinner_infCA.setToolTipText("Select infantry combat ability modifer (100 = 100% of base)");
		
		Label label_discipline = new Label(this, SWT.NONE);
		label_discipline.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_discipline.setToolTipText("Discipline");
		label_discipline.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Discipline.png"));
		
		spinner_disc = new Spinner(this, SWT.BORDER);
		spinner_disc.setDigits(2);
		spinner_disc.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		spinner_disc.setToolTipText("Select discipline modifier (100 = 100% of base value)");
		spinner_disc.setMaximum(300);
		spinner_disc.setSelection((int) (tech.getDiscipline() * 100));
		spinner_disc.addModifyListener(e -> label_totalTactics.setText(Util.DF.format(getTechnology().getTactics())));
		
		Label label_cavCA = new Label(this, SWT.NONE);
		label_cavCA.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_cavCA.setToolTipText("Cavalry combat ability");
		label_cavCA.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Cavalry_power.png"));
		
		spinner_cavCA = new Spinner(this, SWT.BORDER);
		spinner_cavCA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_cavCA.setMaximum(500);
		spinner_cavCA.setSelection((int) (tech.getCavCombatAbility() * 100));
		spinner_cavCA.setToolTipText("Select cavalry combat ability modifer (100 = 100% of base)");
		
		Label label_tactics = new Label(this, SWT.NONE);
		label_tactics.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_tactics.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Military_tactics.png"));
		label_tactics.setToolTipText("Military Tactics");
		label_totalTactics = new Label(this, SWT.NONE);
		label_totalTactics.setToolTipText(
				"Military tactics (Automatically determined from discipline and military technology level)");
		label_totalTactics.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
		label_totalTactics.setText(Util.DF.format(tech.getTactics()));
		
		Label label_artCA = new Label(this, SWT.NONE);
		label_artCA.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, true, true, 1, 1));
		label_artCA.setToolTipText("Artillery combat ability");
		label_artCA.setImage(SWTResourceManager.getImage(TechnologyGroup.class, "/general/Artillery_power.png"));
		
		spinner_artCA = new Spinner(this, SWT.BORDER);
		spinner_artCA.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, true, 1, 1));
		spinner_artCA.setToolTipText("Select artillery combat ability modifer (100 = 100% of base)");
		spinner_artCA.setMaximum(500);
		spinner_artCA.setSelection((int) (tech.getArtCombatAbility() * 100));
		
	}
	
	/**
	 * @return A new {@link Technology} generated from the mil tech level ( {@link #selectedTechGroup}) and spinner
	 *         values
	 */
	public Technology getTechnology() {
		return new Technology(spinner_level.getSelection(), selectedTechGroup, spinner_disc.getSelection() / 100.0,
				spinner_morale.getSelection() / 100.0, spinner_infCA.getSelection() / 100.0,
				spinner_cavCA.getSelection() / 100.0, spinner_artCA.getSelection() / 100.0);
	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
}

package eu4.comsim.gui;

import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.wb.swt.SWTResourceManager;

import eu4.comsim.core.Battle;
import eu4.comsim.core.Regiment;

public class BattleComposite extends Composite {
	
	private static int ICON_PIXEL_DIMENSION = 15;
	
	Battle battle;
	Label[] icons = new Label[160];
	
	/**
	 * Create the composite.
	 *
	 * @param parent
	 * @param style
	 */
	public BattleComposite(Composite parent, int style) {
		super(parent, style);
		setLayout(new GridLayout(40, false));
		icons = new Label[160];
		for (int i = 0; i < 160; i++) {
			icons[i] = createLabel();
		}
	}
	
	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}
	
	private Label createLabel() {
		Label regimentIcon = new Label(this, SWT.BORDER | SWT.CENTER);
		regimentIcon.setAlignment(SWT.CENTER);
		GridData gd_lblNewLabel = new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1);
		gd_lblNewLabel.heightHint = ICON_PIXEL_DIMENSION;
		gd_lblNewLabel.widthHint = ICON_PIXEL_DIMENSION;
		regimentIcon.setLayoutData(gd_lblNewLabel);
		regimentIcon.setVisible(false);
		return regimentIcon;
	}
	
	public void update(Battle b) {
		this.battle = b;
		int width = battle.width;
		
		Arrays.stream(icons).forEach(i -> {
			i.setImage(null);
			i.setVisible(false);
		});
		int offset = (40 - width) / 2;
		fillIcons(battle.battleLineB.backRow, "blue", offset);
		fillIcons(battle.battleLineB.frontRow, "blue", 40 + offset);
		fillIcons(battle.battleLineA.frontRow, "red", 80 + offset);
		fillIcons(battle.battleLineA.backRow, "red", 120 + offset);
		
	}
	
	private void fillIcons(List<Regiment> regiments, String color, int offset) {
		for (int i = 0; i < regiments.size(); i++) {
			Regiment r = regiments.get(i);
			String unitType = r.unit.type.name;
			Image icon = (r == Regiment.EMPTY) ? null
					: SWTResourceManager.getImage(BattleComposite.class, "/battle/" + unitType + "-" + color + ".png");
			icons[offset + i].setImage(icon);
			icons[offset + i].setToolTipText(r.toString());
			icons[offset + i].setVisible(true);
		}
	}
}

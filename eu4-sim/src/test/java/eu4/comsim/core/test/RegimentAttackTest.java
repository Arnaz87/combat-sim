package eu4.comsim.core.test;

import org.junit.Test;

import eu4.comsim.core.Regiment;
import eu4.comsim.core.Technology;
import eu4.comsim.core.datatypes.Phase;
import eu4.comsim.core.datatypes.Unit;

/**
 * Enables testing of individual Regiment attacks
 */
public class RegimentAttackTest {
	
	Technology techA = new Technology(16, Technology.Group.WESTERN, 1.08, 1.293, 1.0, 1.0, 1.0);
	Technology techB = new Technology(16, Technology.Group.WESTERN, 1.05, 1.196, 1.1, 1.0, 1.0);
	
	Regiment infA = new Regiment(0, Unit.DUTCH_MAURICIAN, 1000, techA);
	Regiment infB = new Regiment(0, Unit.DUTCH_MAURICIAN, 1000, techB);
	
	Regiment artA = new Regiment(0, Unit.CHAMBERED_DEMI_CANNON, 1000, techA);
	Regiment artB = new Regiment(0, Unit.CHAMBERED_DEMI_CANNON, 1000, techB);
	
	int baseRoll = 6;
	
	@Test
	public final void testAttack() {
		infA.attack(infB, Phase.FIRE, baseRoll + 1, artB, 1, false);
		artA.attack(infB, Phase.FIRE, baseRoll + 1, artB, 0.5, false);
		// artA.attack(infB, Phase.FIRE, baseRoll + 1, artB, 0.5);
		infB.applyDamage();
		System.out.println("Morale: " + infB.getCurrentMorale());
		System.out.println("Strength: " + infB.getStrength());
	}
}
